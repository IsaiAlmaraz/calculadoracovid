package iab.noxiandev.calculadoracovid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int res, anios;
    String validar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Radio button mujer
        RadioButton mujer = (RadioButton) findViewById(R.id.radioMujer);
        mujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mujer.isChecked()){
                    res = 5;
                }
            }
        });

        // Radio button hombre
        RadioButton hombre = (RadioButton) findViewById(R.id.radioHombre);
        hombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hombre.isChecked()){
                    res = 15;
                }
            }
        });

        // Peso Bajo y Normal no afecta las probabilidades
        RadioButton bajoPeso = (RadioButton) findViewById(R.id.radioBajoP);
        RadioButton normal = (RadioButton) findViewById(R.id.radioNormal);

        // sobrepeso y obesidad afectan
        RadioButton sobrepeso = (RadioButton) findViewById(R.id.radioSobrepeso);
        sobrepeso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sobrepeso.isChecked()){
                    res = res + 10;
                }
            }
        });

        RadioButton obesidad = (RadioButton) findViewById(R.id.radioObesidad);
        obesidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(obesidad.isChecked()){
                    res = res + 10;
                }
            }
        });

        CheckBox pad_1 = (CheckBox) findViewById(R.id.checkPadecimiento1);
        pad_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pad_1.isChecked()){
                    res = res + 5;
                }
            }
        });

        CheckBox pad_2 = (CheckBox) findViewById(R.id.checkPadecimiento2);
        pad_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pad_2.isChecked()){
                    res = res + 15;
                }
            }
        });

        CheckBox pad_3 = (CheckBox) findViewById(R.id.checkPadecimiento3);
        pad_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pad_3.isChecked()){
                    res = res + 5;
                }
            }
        });

        CheckBox pad_4 = (CheckBox) findViewById(R.id.checkPadecimiento4);
        pad_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pad_4.isChecked()){
                    res = res + 25;
                }
            }
        });

        CheckBox pad_5 = (CheckBox) findViewById(R.id.checkPadecimiento5);
        pad_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pad_5.isChecked()){
                    res = res + 10;
                }
            }
        });

        TextView resultados = (TextView) findViewById(R.id.resultados);


        Button calcular = (Button) findViewById(R.id.btnCalcular);
        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edad = (EditText) findViewById(R.id.edad);

                if (mujer.isChecked() != false || hombre.isChecked() != false){
                    validar = edad.getText().toString();
                    if (validar.equals("")){
                        Toast.makeText(MainActivity.this, "Ingresa una edad", Toast.LENGTH_LONG).show();

                    } else{
                        anios = Integer.parseInt(edad.getText().toString());
                        if (anios < 20) {
                            Toast.makeText(MainActivity.this, "La edad tiene que ser más de 20", Toast.LENGTH_LONG).show();
                        }
                        else {
                            for (int i = 20; i <= anios; i++){
                                res = res +1;
                            }

                            if (res <= 30){
                                resultados.setText("Probabilidad Baja");
                                mujer.setChecked(false);
                                hombre.setChecked(false);
                                bajoPeso.setChecked(false);
                                normal.setChecked(false);
                                sobrepeso.setChecked(false);
                                obesidad.setChecked(false);
                                pad_1.setChecked(false);
                                pad_2.setChecked(false);
                                pad_3.setChecked(false);
                                pad_4.setChecked(false);
                                pad_5.setChecked(false);
                                edad.setText("");
                                res = 0;

                            } else if(res > 30 && res <= 50){
                                resultados.setText("Probabilidad Media");
                                mujer.setChecked(false);
                                hombre.setChecked(false);
                                bajoPeso.setChecked(false);
                                normal.setChecked(false);
                                sobrepeso.setChecked(false);
                                obesidad.setChecked(false);
                                pad_1.setChecked(false);
                                pad_2.setChecked(false);
                                pad_3.setChecked(false);
                                pad_4.setChecked(false);
                                pad_5.setChecked(false);
                                edad.setText("");
                                res = 0;

                            } else if (res > 50){
                                resultados.setText("Probabilidad Alta");
                                mujer.setChecked(false);
                                hombre.setChecked(false);
                                bajoPeso.setChecked(false);
                                normal.setChecked(false);
                                sobrepeso.setChecked(false);
                                obesidad.setChecked(false);
                                pad_1.setChecked(false);
                                pad_2.setChecked(false);
                                pad_3.setChecked(false);
                                pad_4.setChecked(false);
                                pad_5.setChecked(false);
                                edad.setText("");
                                res = 0;
                            }
                        }


                    }
                } else {
                    Toast.makeText(MainActivity.this, "Selecciona tu sexo", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}